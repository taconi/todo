from typing import Optional

from sqlmodel import delete, select, update
from todo.database import Session, Status, Todo


def list_todos(
    name: Optional[str],
    description: Optional[str],
    status: Optional[Status],
) -> list[Todo]:
    query = select(Todo).order_by(Todo.updated_at.desc())

    if name:
        query = query.where(Todo.name.contains(name))
    if description:
        query = query.where(Todo.description == description)
    if status:
        query = query.where(Todo.status == status)

    with Session() as session:
        return session.exec(query).all()


def add_todo(todo: Todo):
    with Session() as session:
        session.add(todo)
        session.commit()


def edit_todo(name: str, todo: Todo):
    todo = todo.dict(exclude_unset=True, exclude_none=True)
    statement = update(Todo).where(Todo.name == name).values(**todo)

    with Session() as session:
        session.exec(select(Todo).where(Todo.name == name)).one()

        session.exec(statement)
        session.commit()


def remove_todo(
    name: Optional[str],
    description: Optional[str],
    status: Optional[Status],
):
    query = delete(Todo)
    if name:
        query = query.where(Todo.name == name)
    if description:
        query = query.where(Todo.description == description)
    if status:
        query = query.where(Todo.status == status)

    with Session() as session:
        session.exec(query)
        session.commit()
