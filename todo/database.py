import enum
from contextlib import contextmanager
from datetime import datetime
from pathlib import Path
from uuid import UUID, uuid4
from typing import Optional

from sqlmodel import Session as _Session
from sqlmodel import Field, SQLModel, create_engine

HOME = str(Path.home())
engine = create_engine(f'sqlite:///{HOME}/.todo.db')


class EnumMeta(enum.EnumMeta):
    def __iter__(cls):
        for obj in super().__iter__():
            yield obj.value


class Status(str, enum.Enum, metaclass=EnumMeta):
    todo = 'todo'
    doing = 'doing'
    testing = 'testing'
    done = 'done'

    def colored(self):
        match self:
            case 'todo':
                return f'[red]{self}[/]'
            case 'doing':
                return f'[blue]{self}[/]'
            case 'testing':
                return f'[yellow]{self}[/]'
            case 'done':
                return f'[green]{self}[/]'
            case _:
                return self


class BaseModel(SQLModel):
    """Base model."""

    id: UUID = Field(
        primary_key=True, default_factory=uuid4, index=True, nullable=False
    )
    created_at: datetime = Field(default_factory=datetime.now)
    updated_at: datetime = Field(
        default_factory=datetime.now,
        sa_column_kwargs={'onupdate': datetime.now},
    )
    deleted_at: Optional[datetime]


class Todo(BaseModel, table=True):
    name: str = Field(..., unique=True)
    description: Optional[str]
    status: Status


SQLModel.metadata.create_all(engine)


@contextmanager
def Session():
    with _Session(engine) as session:
        try:
            yield session
        except Exception as exception:
            session.rollback()
            exception.add_note(
                'Exception occurred while database session was open, '
                'a rollback was performed'
            )
            raise exception
