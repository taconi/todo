import rich_click as click
from rich.console import Console
from rich.panel import Panel
from rich.table import Column, Table
from rich.text import Text

from todo.database import Status, Todo
from todo.domain import add_todo, edit_todo, list_todos, remove_todo

console = Console()


@click.group(
    name='todo', context_settings={'help_option_names': ['-h', '--help']}
)
@click.version_option('0.1.0', '-v', '--version')
@click.rich_config(
    help_config=click.RichHelpConfiguration(
        show_arguments=True,
        group_arguments_options=True,
        text_markup='markdown',
    ),
)
def cli():
    """Todo manager."""


@cli.command()
@click.option('-n', '--name', help='Todo name')
@click.option('-d', '--description', help='Todo description')
@click.option(
    '-s', '--status', type=click.Choice(list(Status)), help='Todo status'
)
def ls(
    name: str | None = None,
    description: str | None = None,
    status: Status | None = None,
):
    """List all todos."""
    table = Table(
        Column(header='[blue]Name[/]', style='blue'),
        Column(header='[cyan]Description[/]', max_width=50, style='cyan'),
        Column(header='[wheat1]Status[/]', style='wheat1'),
        Column(header='[magenta]Created At[/]', style='magenta'),
        Column(header='[magenta]Updated At[/]', style='magenta'),
        Column(header='Id'),
        title="TO DO'S",
    )

    for todo in list_todos(name, description, status):
        table.add_row(
            todo.name,
            todo.description,
            Status[todo.status].colored(),
            str(todo.created_at),
            str(todo.updated_at),
            str(todo.id),
        )
    console.print(table)


@cli.command()
@click.option('-n', '--name', required=True, help='Todo name')
@click.option('-d', '--description', help='Todo description')
@click.option(
    '-s',
    '--status',
    required=True,
    type=click.Choice(list(Status)),
    help='Todo status',
)
def add(
    name: str,
    status: Status,
    description: str | None = None,
):
    """Add a todo."""
    todo = Todo(name=name, description=description, status=status)

    try:
        add_todo(todo)
    except Exception as exc:
        panel = Panel.fit(
            Text(f'{exc.args[0]}', justify='center'),
            title='💥 Error 💥',
            style='red',
            border_style='red',
        )
    else:
        panel = Panel.fit(
            Text('Added todo', justify='center'),
            title='✅ Success ✅',
            style='green',
            border_style='green',
        )
    finally:
        console.print(panel)


@cli.command()
@click.argument('old_name', required=True)
@click.option('-n', '--name', help='New todo name')
@click.option('-d', '--description', help='Todo description')
@click.option(
    '-s', '--status', type=click.Choice(list(Status)), help='Todo status'
)
def edit(
    old_name: str,
    name: str | None = None,
    description: str | None = None,
    status: Status | None = None,
):
    """Edit a todo."""
    todo = Todo(name=name, description=description, status=status)

    try:
        edit_todo(old_name, todo)
    except Exception as exc:
        panel = Panel.fit(
            Text(f'{exc.args[0]}', justify='center'),
            title='💥 Error 💥',
            style='red',
            border_style='red',
        )
    else:
        panel = Panel.fit(
            Text('Edited todo', justify='center'),
            title='✅ Success ✅',
            style='green',
            border_style='green',
        )
    finally:
        console.print(panel)


@cli.command()
@click.option('-n', '--name', help=' Todo name')
@click.option('-d', '--description', help='Todo description')
@click.option(
    '-s', '--status', type=click.Choice(list(Status)), help='Todo status'
)
def rm(
    name: str | None = None,
    description: str | None = None,
    status: Status | None = None,
):
    """Remove a todo."""
    if not any([name, description, status]):
        console.print(
            Panel.fit(
                Text('Define at least one option', justify='center'),
                title='⚠️  Warning ⚠️',
                style='yellow',
                border_style='yellow',
            ),
        )
        return

    try:
        remove_todo(name, description, status)
    except Exception as exc:
        panel = Panel.fit(
            Text(f'{exc.args[0]}', justify='center'),
            title='💥 Error 💥',
            style='red',
            border_style='red',
        )
    else:
        panel = Panel.fit(
            Text("Removed to do's", justify='center'),
            title='✅ Success ✅',
            style='green',
            border_style='green',
        )
    finally:
        console.print(panel)
